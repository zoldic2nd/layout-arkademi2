document.addEventListener('DOMContentLoaded', function (){
    const menuToggle = document.querySelector('.hamburger-menu');
    const sideBar = document.querySelector('.sidebar');
    const closeButton = document.querySelector('.close-button');

    menuToggle.addEventListener('click', function (){
        sideBar.classList.toggle('active');
    })
    closeButton.addEventListener('click', function (){
        sideBar.classList.remove('active');
    })
})